package com.webgeekdevcup2014.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.adapters.MovieAdapter;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovie;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieInfo;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieList;
import com.webgeekdevcup2014.retrofit.RetrofitRestHolder;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class ListActivity extends Activity {


    public static final String PARAM_QUERY = "query";

    public static final String PARAM_CATEGORY = "category";

    private ListView lvListResults;

    private String query, category;

    private MovieAdapter movieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.list_activity);

        lvListResults = (ListView) findViewById(R.id.lv_list_results);

        movieAdapter = new MovieAdapter(this, new ArrayList<OmdbMovieInfo>());

        lvListResults.setAdapter(movieAdapter);

        query = getIntent().getStringExtra(PARAM_QUERY);

        category = getIntent().getStringExtra(PARAM_CATEGORY);

        executeQuery(query, category);

    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.abc_fade_out);
    }

    private void executeQuery(String query, String category) {

        if (category.equals(MainActivity.CATEGORY_MOVIE)) {
            queryMovie(query);
        }
    }

    private void queryMovie(String query) {
        RetrofitRestHolder.getOmdbApi().searchMovie(query, new Callback<OmdbMovieList>() {

            @Override
            public void success(OmdbMovieList omdbMovieList, Response response) {

                if (omdbMovieList.Response != null) {
                    Toast.makeText(getApplicationContext(), "No Movies Found.", Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                }

                for (OmdbMovie movie : omdbMovieList.Search) {
                    if (movie.Type.equals("movie")) {
                        RetrofitRestHolder.getOmdbApi().getMovieInfo(movie.imdbID, true, new Callback<OmdbMovieInfo>() {

                            @Override
                            public void success(OmdbMovieInfo omdbMovieInfo, Response response) {
                                if (omdbMovieInfo.Response.equals("True")) {
                                    movieAdapter.addMovie(omdbMovieInfo);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
