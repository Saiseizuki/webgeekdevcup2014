package com.webgeekdevcup2014.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.data.youtube.model.YoutubeSearchVideo;
import com.webgeekdevcup2014.data.youtube.model.YoutubeVideoStastics;
import com.webgeekdevcup2014.retrofit.RetrofitRestHolder;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class MainActivity extends ActionBarActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String CATEGORY_MOVIE = "Movie";
    private static final String[] CATEGORY_LIST = {
            CATEGORY_MOVIE
    };
    private EditText etQuery;
    private Spinner spCategory;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        etQuery    = (EditText) findViewById(R.id.et_query);
        spCategory = (Spinner) findViewById(R.id.sp_category);
        btnSearch = (Button) findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(onClickQuery);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, CATEGORY_LIST);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(adapter);
    }

    private void searchTopViewedVideoThenGetVideoStatistics (String query){
        RetrofitRestHolder.getYoutubeApi().searchVideo(query,new Callback<YoutubeSearchVideo>() {
            @Override
            public void success(YoutubeSearchVideo youtubeSearchVideo, Response response) {
                try {
                    String responseString = convertStreamToString(response.getBody().in());
                    Log.d("WebGeek", responseString);
                    String mostViewsVideoId = youtubeSearchVideo.getItems().get(0).getId().getVideoId();
                    getVideoStatistics(mostViewsVideoId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                try {
                    String responseString = convertStreamToString(retrofitError.getResponse().getBody().in());
                    Log.d("WebGeek", responseString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getVideoStatistics(String videoId){
        RetrofitRestHolder.getYoutubeApi().getVideoStatistics(videoId,new Callback<YoutubeVideoStastics>() {
            @Override
            public void success(YoutubeVideoStastics youtubeVideoStastics, Response response) {
                try {
                    String responseString = convertStreamToString(response.getBody().in());
                    Log.d("WebGeek", "Video Statistics: " + responseString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                try {
                    String responseString = convertStreamToString(retrofitError.getResponse().getBody().in());
                    Log.d("WebGeek", responseString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private View.OnClickListener onClickQuery = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            String query = etQuery.getText().toString();

//            twitterSearchTweet = new TwitterSearchTweet();
//            connectionDetector = new ConnectionDetector(getApplicationContext());
//            if (connectionDetector.isConnectingToInternet()) {
//                twitterSearchTweet.twitterSearch(query, getApplicationContext());
//            }

            if(TextUtils.isEmpty(query)) {
                Toast.makeText(MainActivity.this, "Please Enter Your Query", Toast.LENGTH_SHORT).show();
                return;
            }

            if (spCategory.getSelectedItem().equals("Movie")) {
                queryMovie(query);
            }
        }
    };

    private void queryMovie(String query) {
        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra(ListActivity.PARAM_CATEGORY, CATEGORY_MOVIE);
        intent.putExtra(ListActivity.PARAM_QUERY, query);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.abc_fade_out);
    }
}
