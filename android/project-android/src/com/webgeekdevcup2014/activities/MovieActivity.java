package com.webgeekdevcup2014.activities;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.gda.activitytransition.ActivityTransition;
import com.gda.activitytransition.utils.AnimationUtils;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.squareup.picasso.Picasso;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.adapters.TwitterListViewAdapter;
import com.webgeekdevcup2014.api.twitter.TwitterConstants;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieInfo;
import com.webgeekdevcup2014.data.youtube.model.YoutubeSearchVideo;
import com.webgeekdevcup2014.data.youtube.model.YoutubeSearchVideoItems;
import com.webgeekdevcup2014.data.youtube.model.YoutubeVideoStastics;
import com.webgeekdevcup2014.layouts.MovieDetailsLayout;
import com.webgeekdevcup2014.retrofit.RetrofitRestHolder;
import com.webgeekdevcup2014.views.YoutubeContentLayout;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class MovieActivity extends Activity {

    public static final String TAG = MovieActivity.class.getSimpleName();

    public static OmdbMovieInfo MOVIE_INFO;

    private TextView tvTitle, tvYear;

    private ImageView ivPoster;

    private LinearLayout llAdditionalContent;

    private List<Animator> animatorList = new ArrayList<Animator>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.movie_activity);

        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvYear = (TextView) findViewById(R.id.tv_year);
        ivPoster = (ImageView) findViewById(R.id.iv_poster);
        llAdditionalContent = (LinearLayout) findViewById(R.id.ll_additional_content);

        animatorList.add(AnimationUtils.animateTranslateY(ivPoster, -400, null).setDuration(8000));
        animatorList.add(AnimationUtils.animateTranslateY(ivPoster, 0, null).setDuration(8000));

        ActivityTransition.animateActivityOnEnter(this, this.getClass().getSimpleName(), new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animator) {
                        llAdditionalContent.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        llAdditionalContent.setVisibility(View.VISIBLE);
                        tvTitle.setText(MOVIE_INFO.Title);
                        tvYear.setText(MOVIE_INFO.Year);
                        startContentsAnimation();
                        startPosterAnimation();
                        startAlphaAnimation();
                        Picasso.with(getApplicationContext()).load(MOVIE_INFO.Poster).into(ivPoster);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, android.R.anim.slide_out_right);
        ActivityTransition.removeActivityAnimation(this.getClass().getSimpleName());
    }

    private void startContentsAnimation() {
        MovieDetailsLayout layout = new MovieDetailsLayout(this, MOVIE_INFO);
        llAdditionalContent.addView(layout);
        animateSlide(layout);
        searchTopViewedVideoThenGetVideoStatistics(MOVIE_INFO.Title + " " + MOVIE_INFO.Year + " Movie Trailer");
    }

    public void searchTweets(String searchQuery) {
        AsyncTask<String, Void, Void> asyncTask = new AsyncTask<String, Void, Void>() {

            List<twitter4j.Status> tweets;

            @Override
            protected Void doInBackground(String... querySearch) {
                Log.d(TAG, "twitter search do in background asynctask configuration builder!");
                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                        .setOAuthConsumerKey(TwitterConstants.TWITTER_API_KEY)
                        .setOAuthConsumerSecret(TwitterConstants.TWITTER_API_SECRET)
                        .setOAuthAccessToken(TwitterConstants.TWITTER_ACCESS_TOKEN)
                        .setOAuthAccessTokenSecret(TwitterConstants.TWITTER_ACCESS_TOKEN_SECRET);
                TwitterFactory tf = new TwitterFactory(cb.build());
                Twitter twitter = tf.getInstance();
                Log.d(TAG, "twitter search do in background asynctask query try/catch!");
                try {
                    Query query = new Query("#" + querySearch[0].replaceAll("\\s",""));
                    QueryResult result;
                    result = twitter.search(query);
                    tweets = result.getTweets();
                    Log.d(TAG, "tweet size = " + tweets.size());
                    for (twitter4j.Status tweet : tweets) {
                        Log.d(TAG, tweet.getUser().getName() + " = " + tweet.getUser().getScreenName() + " - "
                                + tweet.getText() + " RetweetCount = " + tweet.getRetweetCount() + " Favorite = "
                                + tweet.getFavoriteCount());
                    }
                } catch (TwitterException te) {
                    te.printStackTrace();
                    Log.d(TAG, "Failed to search tweets: " + te.getMessage());
                }
                Log.d(TAG, "twitter search do in background asynctask return end of function!");
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                TwitterListViewAdapter twitterListViewAdapter = new TwitterListViewAdapter(getApplicationContext(), tweets);
                int i = 0;
                for (twitter4j.Status tweet : tweets) {
                    View view = twitterListViewAdapter.getView(i, null, null);
                    llAdditionalContent.addView(view);
                    animateSlide(view);
                    i++;
                }
                Log.d(TAG, "Twitter ADD VIEW");
            }
        };
        asyncTask.execute(searchQuery);
        Log.d(TAG, "execute query!");
    }

    private void startAlphaAnimation() {
        ObjectAnimator.ofFloat(tvYear, "alpha", 0f, 1f).setDuration(325).start();
        ObjectAnimator.ofFloat(tvTitle, "alpha", 0f, 1f).setDuration(325).start();
        ObjectAnimator.ofFloat(llAdditionalContent, "alpha", 0f, 1f).setDuration(325).start();
    }

    private void startPosterAnimation() {

        AnimatorSet set = AnimationUtils.playSequential(animatorList);

        set.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                startPosterAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        set.start();
    }

    private void animateSlide(View view) {
        Animation animation = android.view.animation.AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        animation.setDuration(200);
        view.startAnimation(animation);
    }


    private void searchTopViewedVideoThenGetVideoStatistics(String query) {

        RetrofitRestHolder.getYoutubeApi().searchVideo(query, new Callback<YoutubeSearchVideo>() {
            @Override
            public void success(YoutubeSearchVideo youtubeSearchVideo, Response response) {
                try {
                    String responseString = convertStreamToString(response.getBody().in());
                    Log.d("WebGeek", responseString);
                    getTopVideoStatistics(youtubeSearchVideo.getItems().get(0));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError retrofitError) {
                try {
                    String responseString = convertStreamToString(retrofitError.getResponse().getBody().in());
                    Log.d("WebGeek", responseString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void getTopVideoStatistics(final YoutubeSearchVideoItems youtubeSearchVideoItems) {

        String videoId = youtubeSearchVideoItems.getId().getVideoId();

        RetrofitRestHolder.getYoutubeApi().getVideoStatistics(videoId, new Callback<YoutubeVideoStastics>() {
            @Override
            public void success(YoutubeVideoStastics youtubeVideoStastics, Response response) {
                try {
                    String responseString = convertStreamToString(response.getBody().in());
                    Log.d("WebGeek", "Video Statistics: " + responseString);
                    YoutubeContentLayout youtubeContentLayout = new YoutubeContentLayout(MovieActivity.this, youtubeVideoStastics, youtubeSearchVideoItems);
                    llAdditionalContent.addView(youtubeContentLayout);
                    animateSlide(youtubeContentLayout);
                    searchTweets(MOVIE_INFO.Title);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                try {
                    String responseString = convertStreamToString(retrofitError.getResponse().getBody().in());
                    Log.d("WebGeek", responseString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
