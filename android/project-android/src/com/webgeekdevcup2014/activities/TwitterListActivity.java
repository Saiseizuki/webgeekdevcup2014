package com.webgeekdevcup2014.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.adapters.TwitterListViewAdapter;
import com.webgeekdevcup2014.api.twitter.TwitterSearchTweet;
import twitter4j.Status;

import java.util.List;

/**
 * Created by jadeantolingaa on 8/17/14.
 */
public class TwitterListActivity extends LinearLayout {
    private ListView mListView;
    private TwitterListViewAdapter mListViewAdapter;
    private List<Status> tweetStatus;
    private View root;
    private LayoutInflater layoutInflater;
    private Activity activity;

    public TwitterListActivity(Context context, List<Status> tweetStatus) {
        super(context);
        this.tweetStatus = tweetStatus;
        layoutInflater = LayoutInflater.from(context);
        root = layoutInflater.inflate(R.layout.twitter_list_view, null, false);
        declareList();
        this.addView(root);
    }

    public void declareList() {
        mListView = (ListView) root.findViewById(R.id.twitter_list_view_linear_layout);
        mListViewAdapter = new TwitterListViewAdapter(getContext(), tweetStatus);
        mListView.setAdapter(mListViewAdapter);
    }
}
