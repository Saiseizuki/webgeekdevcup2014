package com.webgeekdevcup2014.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.gda.activitytransition.ActivityTransition;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.activities.MovieActivity;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieInfo;
import com.webgeekdevcup2014.utilities.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class MovieAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;

    private ArrayList<OmdbMovieInfo> omdbMovieInfos;

    private int animatedPos = 0;

    public MovieAdapter(Context context, ArrayList<OmdbMovieInfo> omdbMovieInfos) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.omdbMovieInfos = omdbMovieInfos;
    }

    public void addMovie(OmdbMovieInfo omdbMovieInfo) {
        omdbMovieInfos.add(omdbMovieInfo);

        Collections.sort(omdbMovieInfos, new Comparator<OmdbMovieInfo>() {

                    @Override
                    public int compare(OmdbMovieInfo o1, OmdbMovieInfo o2) {
                        int x = Integer.parseInt(o1.Year);
                        int y = Integer.parseInt(o2.Year);
                        return y - x;
                    }
                }
        );

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return omdbMovieInfos.size();
    }

    @Override
    public Object getItem(int i) {
        return omdbMovieInfos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int pos, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = inflater.inflate(R.layout.list_activity_video_item, null);
            ViewHolder temp = new ViewHolder();

            temp.director = (TextView) view.findViewById(R.id.tv_director);
            temp.poster = (ImageView) view.findViewById(R.id.iv_poster);
            temp.title = (TextView) view.findViewById(R.id.tv_title);
            temp.genre = (TextView) view.findViewById(R.id.tv_genre);
            temp.year = (TextView) view.findViewById(R.id.tv_year);
            temp.imdbRating = (TextView) view.findViewById(R.id.tv_imdb_rating);
            temp.rottenTomatoRating = (TextView) view.findViewById(R.id.tv_rotten_tomato_rating);

            view.setTag(temp);
        }

        OmdbMovieInfo movieInfo = omdbMovieInfos.get(pos);
        final ViewHolder holder = (ViewHolder) view.getTag();

        holder.title.setText(movieInfo.Title);
        holder.year.setText(movieInfo.Year);
        holder.director.setText("Director: " + movieInfo.Director);
        holder.genre.setText("Genre: " + movieInfo.Genre);
        holder.imdbRating.setText("IMDB: " + movieInfo.imdbRating);
        holder.rottenTomatoRating.setText("RottenTomato: " + movieInfo.tomatoRating);

        Picasso.with(context).load(movieInfo.Poster).placeholder(R.drawable.placeholder).into(holder.poster, new Callback() {

            @Override
            public void onSuccess() {
                ((View) holder.poster.getParent()).setBackgroundColor(Utils.getColorFromImageView(holder.poster));
            }

            @Override
            public void onError() {
            }
        });

        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                MovieActivity.MOVIE_INFO = omdbMovieInfos.get(pos);
                Intent intent = new Intent(context, MovieActivity.class);
                Bitmap bitmap = null;

                try {
                    bitmap = ((BitmapDrawable) holder.poster.getDrawable()).getBitmap();
                }
                catch (NullPointerException ignored) {
                }

                ActivityTransition.addActivityAnimation((android.app.Activity) context, MovieActivity.class.getSimpleName(), holder.poster, bitmap);
                context.startActivity(intent);
            }
        });

        if (animatedPos == pos) {
            animatedPos ++;
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            animation.setDuration(200);
            view.startAnimation(animation);
        }

        return view;
    }

    static class ViewHolder {

        TextView title;

        TextView year;

        TextView director;

        TextView genre;

        TextView imdbRating;

        TextView rottenTomatoRating;

        ImageView poster;
    }
}
