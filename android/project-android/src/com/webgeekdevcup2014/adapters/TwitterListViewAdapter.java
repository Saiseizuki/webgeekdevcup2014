package com.webgeekdevcup2014.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.webgeekdevcup2014.R;
import twitter4j.Status;

import java.util.List;

/**
 * Created by jadeantolingaa on 8/17/14.
 */
public class TwitterListViewAdapter extends BaseAdapter {

    private List<Status> tweetStatus;
    private Context context;

    public TwitterListViewAdapter(Context context, List<Status> tweetStatus) {
        this.context = context;
        this.tweetStatus = tweetStatus;
    }

    @Override
    public int getCount() {
        return tweetStatus.size();
    }

    @Override
    public Object getItem(int position) {
        return tweetStatus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tweetStatus.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parentView) {
        TwitterListViewAdapterHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.twitter_list_view_item, parentView, false);
            viewHolder = new TwitterListViewAdapterHolder();
            viewHolder.mImageViewProfile = (ImageView) convertView.findViewById(R.id
                    .twitter_list_view_item_relative_layout_image_view);
            viewHolder.mTextViewScreenName = (TextView) convertView.findViewById(R.id
                    .twitter_list_view_item_relative_layout_text_view_username);
            viewHolder.mTextViewTweet = (TextView) convertView.findViewById(R.id
                    .twitter_list_view_item_relative_layout_text_view_tweet);
            viewHolder.mTextViewRetweet = (TextView) convertView.findViewById(R.id
                    .twitter_list_view_item_relative_layout_text_view_retweet_count);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TwitterListViewAdapterHolder) convertView.getTag();
        }
        Picasso.with(context).load(tweetStatus.get(position).getUser().getBiggerProfileImageURL()).into(viewHolder
                .mImageViewProfile);
        viewHolder.mTextViewScreenName.setText(tweetStatus.get(position).getUser().getScreenName());
        viewHolder.mTextViewTweet.setText(tweetStatus.get(position).getText());
        viewHolder.mTextViewRetweet.setText("Retweet: " + tweetStatus.get(position).getRetweetCount() + "    Favorite: "
                + tweetStatus.get(position).getFavoriteCount());
        return convertView;
    }

    class TwitterListViewAdapterHolder {
        ImageView mImageViewProfile;
        TextView mTextViewScreenName;
        TextView mTextViewTweet;
        TextView mTextViewRetweet;
    }
}