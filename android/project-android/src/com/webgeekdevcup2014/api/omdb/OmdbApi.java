package com.webgeekdevcup2014.api.omdb;

import com.webgeekdevcup2014.data.omdb.model.OmdbMovieInfo;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieList;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by gdaAquino on 8/16/14.
 */
public interface OmdbApi {

    @GET("/")
    public void searchMovie(@Query("s") String movieTitle, Callback<OmdbMovieList> movieListCallback);

    @GET("/")
    public void getMovieInfo(@Query("i") String movieId, @Query("tomatoes") boolean tomatoes, Callback<OmdbMovieInfo> movieCallback);

}
