package com.webgeekdevcup2014.api.twitter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.webgeekdevcup2014.Utilities.ConnectionDetector;
import com.webgeekdevcup2014.activities.TwitterListActivity;
import com.webgeekdevcup2014.views.YoutubeContentLayout;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.List;

/**
 * Created by jadeantolingaa on 8/16/14.
 */
public class TwitterSearchTweet {
    public static final String TAG = TwitterSearchTweet.class.getSimpleName();


    public void twitterSearch(String searchQuery, final Context context) {
        AsyncTask<String, String, Void> asyncTask = new AsyncTask<String, String, Void>() {
            @Override
            protected Void doInBackground(String... querySearch) {
                Log.d(TAG, "twitter search do in background asynctask configuration builder!");
                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                        .setOAuthConsumerKey(TwitterConstants.TWITTER_API_KEY)
                        .setOAuthConsumerSecret(TwitterConstants.TWITTER_API_SECRET)
                        .setOAuthAccessToken(TwitterConstants.TWITTER_ACCESS_TOKEN)
                        .setOAuthAccessTokenSecret(TwitterConstants.TWITTER_ACCESS_TOKEN_SECRET);
                TwitterFactory tf = new TwitterFactory(cb.build());
                Twitter twitter = tf.getInstance();
                Log.d(TAG, "twitter search do in background asynctask query try/catch!");
                try {
                    Query query = new Query("#"+querySearch[0]);
                    QueryResult result;
                    result = twitter.search(query);
                    List<twitter4j.Status> tweets = result.getTweets();
                    TwitterListActivity twitterListActivity= new TwitterListActivity(context, tweets);
                    //llAdditionalContent.addView(twitterListActivity);
                    for (twitter4j.Status tweet : tweets) {

                        Log.d(TAG, tweet.getUser().getName() + " = " + tweet.getUser().getScreenName() + " - "
                                + tweet.getText() + " RetweetCount = " + tweet.getRetweetCount() + " Favorite = "
                                + tweet.getFavoriteCount() );
                    }
                } catch (TwitterException te) {
                    te.printStackTrace();
                    Log.d(TAG,"Failed to search tweets: " + te.getMessage());
                }
                Log.d(TAG, "twitter search do in background asynctask return end of function!");
                return null;
            }
        };
        asyncTask.execute(searchQuery);
        Log.d(TAG, "execute query!");
    }

}
