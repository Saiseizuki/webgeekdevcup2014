package com.webgeekdevcup2014.api.youtube;

import com.webgeekdevcup2014.data.youtube.model.YoutubeSearchVideo;
import com.webgeekdevcup2014.data.youtube.model.YoutubeVideoStastics;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by xytan on 8/16/14.
 */
public interface YoutubeApi {

    @GET("/search?part=snippet&orderBy=viewCount&key="+YoutubeConstants.YOUTUBE_ANDROID_API_KEY)
    public void searchVideo(@Query("q")String q, Callback<YoutubeSearchVideo> youtubeSearchVideoCallback);

    @GET("/videos?part=statistics&key="+YoutubeConstants.YOUTUBE_ANDROID_API_KEY)
    public void getVideoStatistics(@Query("id")String id, Callback<YoutubeVideoStastics> youtubeVideoStasticsCallback);

}
