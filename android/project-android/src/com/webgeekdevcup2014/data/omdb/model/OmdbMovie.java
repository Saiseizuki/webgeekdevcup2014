package com.webgeekdevcup2014.data.omdb.model;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class OmdbMovie {

    public String Title;

    public String Year;

    public String imdbID;

    public String Type;
}
