package com.webgeekdevcup2014.data.omdb.model;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class OmdbMovieInfo {

    public String Title;

    public String Year;

    public String Rated;

    public String Released;

    public String Runtime;

    public String Director;

    public String Writer;

    public String Actors;

    public String Genre;

    public String Plot;

    public String Language;

    public String Country;

    public String Awards;

    public String Poster;

    public String Metascore;

    public String imdbRating;

    public String imdbVotes;

    public String imdbID;

    public String tomatoRating;

    public String Type;

    public String Response;

}
