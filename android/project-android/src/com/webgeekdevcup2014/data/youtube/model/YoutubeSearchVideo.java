package com.webgeekdevcup2014.data.youtube.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xytan on 8/16/14.
 */
public class YoutubeSearchVideo {
    String nextPageToken;
    List<YoutubeSearchVideoItems> items;
    String kind;

    public List<YoutubeSearchVideoItems> getItems() {
        return items;
    }

    String etag;
    PageInfo pageInfo;


    public static class PageInfo {
        String totalResults;
        String resultsPerPage;
    }
}
