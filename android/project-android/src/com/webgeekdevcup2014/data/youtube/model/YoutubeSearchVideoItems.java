package com.webgeekdevcup2014.data.youtube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xytan on 8/16/14.
 */
public class YoutubeSearchVideoItems {
    Id id;
    Snippet snippet;
    String kind;
    String etag;

    public Id getId() {
        return id;
    }

    public static class Id {
        String videoId;
        String kind;

        public String getVideoId() {
            return videoId;
        }
    }

    public static class Snippet {
        Thumbnails thumbnails;
        String channelId;
        String title;
        String publishedAt;
        String description;
        String liveBroadcastContent;
        String channelTitle;

        public Thumbnails getThumbnails() {
            return thumbnails;
        }

        public String getChannelId() {
            return channelId;
        }

        public String getTitle() {
            return title;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public String getDescription() {
            return description;
        }

        public String getLiveBroadcastContent() {
            return liveBroadcastContent;
        }

        public String getChannelTitle() {
            return channelTitle;
        }

        public static class Thumbnails {

            Medium medium;
            High high;
            @SerializedName("default")
            Default _default;

            public Medium getMedium() {
                return medium;
            }

            public High getHigh() {
                return high;
            }

            public Default get_default() {
                return _default;
            }

            public static class Medium {

                String url;

                public String getUrl() {
                    return url;
                }
            }

            public static class High {

                String url;

                public String getUrl() {
                    return url;
                }
            }

            public static class Default {

                String url;

                public String getUrl() {
                    return url;
                }
            }
        }
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public String getKind() {
        return kind;
    }

    public String getEtag() {
        return etag;
    }
}
