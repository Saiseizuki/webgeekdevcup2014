package com.webgeekdevcup2014.data.youtube.model;

import java.util.List;

/**
 * Created by xytan on 8/16/14.
 */
public class YoutubeVideoStastics {
    String kind;
    String etag;
    PageInfo pageInfo;

    List<YoutubeVideoStatisticsItems> items;

    public static class PageInfo{
        String totalResults;
        String resultsPerPage;
    }

    public String getKind() {
        return kind;
    }

    public String getEtag() {
        return etag;
    }

    public List<YoutubeVideoStatisticsItems> getItems() {
        return items;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }
}
