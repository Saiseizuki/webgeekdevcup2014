package com.webgeekdevcup2014.data.youtube.model;

/**
 * Created by xytan on 8/16/14.
 */
public class YoutubeVideoStatisticsItems {
    String kind;
    String etag;
    String id;
    Statistics statistics;

    public static class Statistics{
        String viewCount;
        String likeCount;
        String dislikeCount;
        String favoriteCount;
        String commentCount;

        public String getViewCount() {
            return viewCount;
        }

        public String getLikeCount() {
            return likeCount;
        }

        public String getDislikeCount() {
            return dislikeCount;
        }

        public String getFavoriteCount() {
            return favoriteCount;
        }

        public String getCommentCount() {
            return commentCount;
        }
    }

    public String getKind() {
        return kind;
    }

    public String getEtag() {
        return etag;
    }

    public String getId() {
        return id;
    }

    public Statistics getStatistics() {
        return statistics;
    }
}
