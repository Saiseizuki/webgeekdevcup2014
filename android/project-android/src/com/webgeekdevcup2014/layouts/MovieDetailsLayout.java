package com.webgeekdevcup2014.layouts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.data.omdb.model.OmdbMovieInfo;

/**
 * Created by gdaAquino on 8/17/14.
 */
public class MovieDetailsLayout extends RelativeLayout {

    private TextView tvPlot;

    private TextView tvActors;

    public MovieDetailsLayout(Context context, OmdbMovieInfo omdbMovieInfo) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.movie_activity_movie_details, null);
        tvPlot = (TextView) view.findViewById(R.id.tv_plot);
        tvActors = (TextView) view.findViewById(R.id.tv_actors);
        tvPlot.setText(omdbMovieInfo.Plot);
        tvActors.setText(omdbMovieInfo.Actors);
        addView(view);
    }
}
