package com.webgeekdevcup2014.retrofit;

import com.webgeekdevcup2014.api.omdb.OmdbApi;
import com.webgeekdevcup2014.api.omdb.OmdbConstants;
import com.webgeekdevcup2014.api.youtube.YoutubeApi;
import com.webgeekdevcup2014.api.youtube.YoutubeConstants;

import retrofit.RestAdapter;

/**
 * Created by gdaAquino on 8/16/14.
 */
public class RetrofitRestHolder {

    private static OmdbApi omdbApi;
    private static YoutubeApi youtubeApi;

    public static OmdbApi getOmdbApi() {
        if (omdbApi == null) {
            omdbApi = new RestAdapter.Builder()
                    .setEndpoint(OmdbConstants.OmdbEndpoint)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build()
                    .create(OmdbApi.class);
        }
        return omdbApi;
    }

    public static YoutubeApi getYoutubeApi(){
        if (youtubeApi == null) {
            youtubeApi= new RestAdapter.Builder()
                    .setEndpoint(YoutubeConstants.YoutubeEndpoint)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build()
                    .create(YoutubeApi.class);
        }
        return youtubeApi;
    }
}
