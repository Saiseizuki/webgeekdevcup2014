package com.webgeekdevcup2014.utilities;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

/**
 * Created by gdaAquino on 8/17/14.
 */
public class Utils {

    public static int getColorFromImageView(ImageView view) {
        try {
            Bitmap bitmap = ((BitmapDrawable) view.getDrawable()).getBitmap();
            int color = bitmap.getPixel(0, 0);
            return Color.argb(75, Color.red(color), Color.green(color), Color.blue(color));
        }
        catch (NullPointerException ignored) {
            return 0;
        }
    }
}
