package com.webgeekdevcup2014.views;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webgeekdevcup2014.R;
import com.webgeekdevcup2014.data.youtube.model.YoutubeSearchVideoItems;
import com.webgeekdevcup2014.data.youtube.model.YoutubeVideoStastics;
import com.webgeekdevcup2014.utilities.IntentUtilities;

/**
 * Created by xytan on 8/16/14.
 */
public class YoutubeContentLayout extends LinearLayout {

    YoutubeVideoStastics youtubeVideoStastics;
    YoutubeSearchVideoItems topVideo;
    ImageView videoThumbnail;
    TextView dislikeTextView,likeTextView,viewCountTextView;
    ProgressBar likeDislikeBar;
    View root;
    Context context;
    String id;

    public YoutubeContentLayout(Context context, YoutubeVideoStastics youtubeVideoStastics,YoutubeSearchVideoItems topVideo){
            super(context);
        this.youtubeVideoStastics=youtubeVideoStastics;
        this.topVideo =topVideo;
        this.context = context;
        this.id = topVideo.getId().getVideoId();
        LayoutInflater inflater = LayoutInflater.from(context);
        root = inflater.inflate(R.layout.youtube_content_layout,null,false);
        Log.d("WebGeek2", "inflate success");
        init();
        this.addView(root);
    }

    private void init(){
        dislikeTextView = (TextView)root.findViewById(R.id.video_dislikes);
        likeTextView = (TextView)root.findViewById(R.id.video_likes);
        viewCountTextView=(TextView)root.findViewById(R.id.video_views_count);
        likeDislikeBar = (ProgressBar)root.findViewById(R.id.like_dislike_bar);
        videoThumbnail = (ImageView)root.findViewById(R.id.video_thumbnail);


        int likes = Integer.parseInt(youtubeVideoStastics.getItems().get(0).getStatistics().getLikeCount());
        int dislikes = Integer.parseInt(youtubeVideoStastics.getItems().get(0).getStatistics().getDislikeCount());
        int total = likes+dislikes;

        int viewCount = Integer.parseInt(youtubeVideoStastics.getItems().get(0).getStatistics().getViewCount());

        Log.d("WebGeek",likes +" "+dislikes + " "+ viewCount);
        likeDislikeBar.setMax(total);
        likeDislikeBar.setProgress(likes);

        viewCountTextView.setText(viewCount+" views");
        dislikeTextView.setText(dislikes+" dislikes");
        likeTextView.setText(likes+" likes");


        String thumbnailUrl = topVideo.getSnippet().getThumbnails().getHigh().getUrl();

        Log.d("UriGaming",thumbnailUrl);
        Picasso.with(getContext().getApplicationContext()).load(Uri.parse(thumbnailUrl)).placeholder(R.drawable.placeholder).into(videoThumbnail);

        videoThumbnail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentUtilities.launchYoutubeVideoActivity(context,id);
            }
        });

    }


}
