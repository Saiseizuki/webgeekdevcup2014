module.exports = {
    _config: {},
    getAllUsers: function(req, res) {
        Users.find(function(err, users) {
            if (err) return res.json(err);
            res.json(users);
        });
    },
    addUsers: function(req, res) {
        Users.create(req.body).done(function(err, users) {
            if (err) return res.json(err);
                res.json(users);
        });
    },
    getUser: function(req, res) {
        //var id = req.param('user_id');
        //Users.find({ id: req.param.user_id }).done(function(err, users) {
        Users.findOne({ id: req.params.user_id }).done(function(err, users) {
            if (err) return res.json(err);
            res.json(users);
        });
    },
    deleteUser: function(req, res) {
        Users.destroy({ id: req.params.user_id }).done(function(err, users) {
            if (err) return res.json(err);
            res.json(users);
        });
    },
    updateUser: function(req, res) {
        Users.update({ id: req.params.user_id }, req.body, function(err, users) {
            if (err) return res.json(err);
            res.json(users);
        });
    }
};
