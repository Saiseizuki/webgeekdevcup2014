module.exports = {
	attributes: {
		userId: {
        	type: 'STRING',
        	required: true
    	},
    	productName: {
	    	type: 'STRING',
	    	required: true
		},
		imageLink: {
	    	type: 'STRING',
	    	required: true
		},
		description: {
	    	type: 'STRING',
	    	required: true
		},
		category: {
	    	type: 'STRING',
	    	required: true
		},
		availableStock: {
	    	type: 'INTEGER',
	    	required: true
		},
		price: {
			type: 'INTEGER',
			required: true
		}
    }  
};
