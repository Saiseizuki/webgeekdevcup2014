module.exports = {
    attributes: {
    userName: {
        type: 'STRING',
        required: true
    },
    password: {
	    type: 'STRING',
	    required: true
	},
	firstName: {
	    type: 'STRING',
	    required: true
	},
	lastName: {
	    type: 'STRING',
	    required: true
	},
	email: {
	    type: 'STRING',
	    required: true
	},
	storeName: {
	    type: 'STRING',
	    required: true
	}
    }
};
